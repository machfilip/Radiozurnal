# Radiožurnál web scraper

This project is about downloading regular broadcasts from Radiozurnal (public radio service in Czech republic) to local drive

## Incremental downloads
Purpuse is:
1. Try to find latest record
2. Check if its has been already downloaded
3. Download record which is new and is not present in database
4. Reduce bitrate of downloaded file because of saving space on drive
5. Rsync local folder to OneDrive
6. Rsync OneDrive to Android device (ie. via FreeSync)
7. Enjoy listening your favorite podcasts :)

## Downloading whole archive:
There exists two files - *py_radiozurnal_download.py* and *py_radiozurnal_download_krimi.py*. These files have different purpose because they download whole archive of existing broadcast series to local drive.

1. Go through webpage
2. Find exact html tag with specified parameters
3. Extract mp3 link
4. Download file to local drive
5. Enjoy listening

## Requirements:
1. Create file called application.yml in upper folder called "conf" (or change config in appropriate way)
2. Configure it regarding your credentials for database and email server ie:
```
gmail:
    username: gmail_username@gmail.com
    password: your_generated_secret_password
aws:
    radiozurnal:
        username: your_username
        password: your_password
        database: your_databasename
        host: your_host
        port: your_host_port
```

