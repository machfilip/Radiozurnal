import urllib
from urllib.parse import urlparse
import urllib.request
import re
from bs4 import BeautifulSoup
import mysql.connector
import sys
import os
from datetime import datetime
import syslog
import subprocess
import yaml
from inspect import getsourcefile
import ssl

syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
syslog.syslog('Jak to vidi')

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['radiozurnal']['username']
secret_password = conf['aws']['radiozurnal']['password']
secret_database = conf['aws']['radiozurnal']['database']
secret_host = conf['aws']['radiozurnal']['host']
secret_port = conf['aws']['radiozurnal']['port']
def_path = conf['aws']['radiozurnal']['directory']['jak_to_vidi']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

url = 'https://hledani.rozhlas.cz/iRadio/?query=&reader=&stanice%5B%5D=%C4%8CRo+Dvojka&porad%5B%5D=Jak+to+vid%C3%AD&offset=0&canDownload=0'

category = 'Jak to vidi'

context = ssl._create_unverified_context()

with urllib.request.urlopen(url,context=context) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")

#print soup

links = soup.find_all('ul', 'box-audio-archive')

#enclosure
for tag in links:
	title = tag.find('div','title').get_text()
	link = tag.find('div','action action-download').a.get('href')

	query = ("SELECT * FROM radiozurnal.Record WHERE LINK = %s")
	cursor.execute(query, (link,) )

	rows = cursor.fetchall()

	success = 0

	if cursor.rowcount < 1:

		mp3file = urllib.request.urlopen(link)
		pattern = re.compile('[0-9]+\.[0-9]+\.[0-9]+\s[0-9]+:[0-9]+[A-Za-z0-9ábčďéěíňñóřšťúůýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ:,\s]+', re.UNICODE)
		filename = pattern.match(title).group(0)
		pattern = re.compile('[0-9]+\.[0-9]+\.[0-9]+', re.UNICODE)
		datum = pattern.match(title).group(0)
		datum = datetime.strptime(datum, '%d.%m.%Y')
		datum = str(datum.strftime('%Y-%m-%d'))

		filename = re.sub(r'[0-9]+:[0-9]+', '-', filename)
		filename = re.sub(r'[0-9]+\.[0-9]+\.[0-9]+', '', filename)
		file = def_path+'/'+datum+filename+'.mp3'
		file = file.replace(":", "")

		query = "INSERT INTO radiozurnal.Record (TITLE, LINK, CATEGORY) VALUES (%s, %s, %s)"

		try:
			with open(file,'wb+') as output:
				output.write(mp3file.read())

			syslog.syslog('Downloading file')
			success = 1

		except Exception as error:
			file = def_path+'/'+datum+filename+'.mp3'

			try:
				with open(file,'wb+') as output:
					output.write(mp3file.read())

				syslog.syslog('Downloading file')
				success = 1

			except:
				syslog.syslog('Error: ', sys.exc_info()[0])

		if success != 0:
			cursor.execute(query, (title, link, category))
			cnx.commit()

cursor.close()
cnx.close()

sys.exit()
