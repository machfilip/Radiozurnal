# -*- coding: utf-8 -*-
import urllib2
import re
from bs4 import BeautifulSoup
import mysql.connector
import sys
from datetime import datetime
import syslog
import subprocess

syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
syslog.syslog('Pribehy 20. stoleti')

offset = 0

while offset < 500:

	url = 'http://hledani.rozhlas.cz/iRadio/?query=&reader=&stanice%5B%5D=%C4%8CRo+Plus&porad%5B%5D=P%C5%99%C3%ADb%C4%9Bhy+20.+stolet%C3%AD&offset='+str(offset)+'&sort=docdatetime&canDownload=0'
	#print(url)

	category = 'Pribehy_20_stoleti'

	conn = urllib2.urlopen(url)
	html = conn.read()

	soup = BeautifulSoup(html, "html.parser")

	links = soup.find_all('ul', 'box-audio-archive')

#enclosure
	for tag in links:

		try:
			title = tag.find('div','title').get_text()
			link = tag.find('div','action action-download').a.get('href')

			success = 0

			if link:

				mp3file = urllib2.urlopen(link)
				pattern = re.compile(ur'[0-9]+\.[0-9]+\.[0-9]+\s[0-9]+:[0-9]+[A-Za-z0-9ábčďéěíňñóřšťúůýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ,\s]+', re.UNICODE)
				filename = pattern.match(title).group(0)
				pattern = re.compile(ur'[0-9]+\.[0-9]+\.[0-9]+', re.UNICODE)
				datum = pattern.match(title).group(0)
				datum = datetime.strptime(datum, '%d.%m.%Y')
				datum = str(datum.strftime('%Y-%m-%d'))

				filename = re.sub(r'[0-9]+:[0-9]+', '-', filename)
				filename = re.sub(r'[0-9]+\.[0-9]+\.[0-9]+', '', filename)
				file = "/home/filip/Music/Radiozurnal/"+category+'/'+datum+filename[:200]+".mp3"

				print file
				print link

				try:
					with open(file, "w") as outfile:
						outfile.write(mp3file.read())
				except IOError:
					syslog.syslog('Error occured: ', sys.exc_info()[0])
		except:
			syslog.syslog('Error occured: ', sys.exc_info()[0])
	offset = offset +10
