def czech_to_unicode(str_input):
	import unicodedata
	line=str_input
	line = unicodedata.normalize('NFKD', line)
	output = ''
	for c in line:
		if not unicodedata.combining(c):
			output += c
	return output
